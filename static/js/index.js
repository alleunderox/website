/* global ReactDOM, fetch, Headers, Request, MainPage, MaterialPage, OnlinePage, TrailersPage */
'use strict';

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

document.addEventListener('DOMContentLoaded', function () {
  ReactDOM.render(React.createElement(ReactRouterDOM.BrowserRouter, null, React.createElement("div", {
    className: "hero is-black is-bold is-fullheight"
  }, React.createElement("div", {
    className: "hero-head"
  }, React.createElement("div", {
    className: "container"
  }, React.createElement(ReactRouterDOM.Route, {
    strict: true,
    sensitive: true,
    path: "/query/:query/online/",
    component: OnlinePage
  }))), React.createElement("div", {
    className: "hero-body"
  }, React.createElement("div", {
    className: "container"
  }, React.createElement(ReactRouterDOM.Route, {
    strict: true,
    sensitive: true,
    exact: true,
    path: "/",
    component: MainPage
  }), React.createElement(ReactRouterDOM.Route, {
    strict: true,
    sensitive: true,
    exact: true,
    path: "/query/:query/",
    component: MaterialPage
  }), React.createElement(ReactRouterDOM.Route, {
    strict: true,
    sensitive: true,
    exact: true,
    path: "/query/:query/trailer/",
    component: TrailersPage
  }))))), document.getElementById('react-dom'));
});
var storage = {
  'material': null,
  'materials': null,
  'trailers': null,
  'sources': null,
  'error': null,
  'hasEpisodes': null,
  'query': null
};

var getMaterialsByName = function getMaterialsByName(parent, query) {
  parent.setState({
    'showLoading': true,
    'showError': false
  });
  var body = JSON.stringify({
    query: query
  });
  var headers = new Headers({
    'Content-Type': 'application/json'
  });
  var request = new Request('/api/getMaterialsByName', {
    'method': 'POST',
    'cache': 'no-store',
    'body': body,
    'headers': headers
  });
  fetch(request).then(function (response) {
    if (response.ok === true) {
      return response.json();
    }

    return {
      'error': {
        'text': '...'
      }
    };
  }).then(function (data) {
    if (data.hasOwnProperty('error') === true) {
      storage.error = data.error.text;
      parent.setState({
        'showLoading': false,
        'showError': true,
        'showResults': false
      });
    } else {
      storage.materials = data.materials;
      parent.setState({
        'showLoading': false,
        'showError': false,
        'showResults': true
      });
    }
  });
};

var getMaterialById = function getMaterialById(parent, query) {
  var wait = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  var body = JSON.stringify({
    query: query
  });
  var headers = new Headers({
    'Content-Type': 'application/json'
  });
  var request = new Request('/api/getMaterialById', {
    'method': 'POST',
    'cache': 'no-store',
    'body': body,
    'headers': headers
  });
  return fetch(request).then(function (response) {
    if (response.ok === true) {
      return response.json();
    }

    return {
      'error': {
        'text': '...'
      }
    };
  }).then(function (data) {
    if (data.hasOwnProperty('error') === true) {
      storage.error = data.error;
      parent.setState({
        'showLoading': false,
        'showError': true
      });
    } else {
      storage.material = data.material;

      if (wait === false) {
        parent.setState({
          'showLoading': false,
          'showError': false
        });
      }
    }
  });
};

var getYouTubeTrailersByName = function getYouTubeTrailersByName(parent, query) {
  var body = JSON.stringify({
    query: query
  });
  var headers = new Headers({
    'Content-Type': 'application/json'
  });
  var request = new Request('/api/getYouTubeTrailersByName', {
    'method': 'POST',
    'cache': 'no-store',
    'body': body,
    'headers': headers
  });
  return fetch(request).then(function (response) {
    if (response.ok === true) {
      return response.json();
    }

    return {
      'error': {
        'text': '...'
      }
    };
  }).then(function (data) {
    if (data.hasOwnProperty('error') === true) {
      storage.error = data.error;
      parent.setState({
        'showLoading': false,
        'showError': true
      });
    } else {
      storage.trailers = data.trailers;
      parent.setState({
        'showLoading': false,
        'showError': false
      });
    }
  });
};

var getSourcesById = function getSourcesById(parent, query) {
  var body = JSON.stringify({
    query: query
  });
  var headers = new Headers({
    'Content-Type': 'application/json'
  });
  var request = new Request('/api/getSourcesById', {
    'method': 'POST',
    'cache': 'no-store',
    'body': body,
    'headers': headers
  });
  return fetch(request).then(function (response) {
    if (response.ok === true) {
      return response.json();
    }

    return {
      'error': {
        'text': '...'
      }
    };
  }).then(function (data) {
    if (data.hasOwnProperty('error') === true) {
      storage.error = data.error;
      parent.setState({
        'showLoading': false,
        'showError': true
      });
    } else {
      storage.sources = data.sources;
      storage.hasEpisodes = data.sources[0].translators[0].hasOwnProperty('seasons') === true;
      parent.setState({
        'showLoading': false,
        'showError': false
      });
    }
  });
};
/* global React */


var Episode =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Episode, _React$Component);

  function Episode() {
    _classCallCheck(this, Episode);

    return _possibleConstructorReturn(this, _getPrototypeOf(Episode).apply(this, arguments));
  }

  _createClass(Episode, [{
    key: "render",
    value: function render() {
      return React.createElement("li", null, React.createElement(ReactRouterDOM.NavLink, {
        strict: true,
        sensitive: true,
        activeClassName: "is-active",
        to: this.props.to
      }, React.createElement("p", {
        className: "subtitle"
      }, this.props.episode.episode, " \u0441\u0435\u0440\u0438\u044F")));
    }
  }]);

  return Episode;
}(React.Component);
/* global React */


var Episodes =
/*#__PURE__*/
function (_React$Component2) {
  _inherits(Episodes, _React$Component2);

  function Episodes(props) {
    var _this;

    _classCallCheck(this, Episodes);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Episodes).call(this, props));
    _this.state = {
      'query': -1
    };
    _this.onInputChange = _this.onInputChange.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Episodes, [{
    key: "onInputChange",
    value: function onInputChange(event) {
      var query = parseInt(event.target.value.trim());
      this.setState({
        'query': Number.isNaN(query) === true ? -1 : query
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return React.createElement(Menu, {
        className: "column is-2 animated slideInDown",
        title: "\u0421\u0435\u0440\u0438\u0438",
        function: this.onInputChange
      }, this.props.episodes.map(function (episode, episodeIndex) {
        return episode.episode >= _this2.state.query && React.createElement(Episode, {
          to: "".concat(_this2.props.pathname).concat(episodeIndex, "/"),
          episode: episode
        });
      }));
    }
  }]);

  return Episodes;
}(React.Component);
/* global React */


var Error =
/*#__PURE__*/
function (_React$Component3) {
  _inherits(Error, _React$Component3);

  function Error() {
    _classCallCheck(this, Error);

    return _possibleConstructorReturn(this, _getPrototypeOf(Error).apply(this, arguments));
  }

  _createClass(Error, [{
    key: "render",
    value: function render() {
      return React.createElement("div", {
        className: "modal is-active"
      }, React.createElement("div", {
        className: "modal-background"
      }), React.createElement("div", {
        className: "modal-content animated bounce"
      }, React.createElement("div", {
        className: "notification is-danger"
      }, React.createElement("p", {
        className: "title"
      }, "\u0423\u043F\u0441..."), React.createElement("p", null, this.props.error.text))));
    }
  }]);

  return Error;
}(React.Component);
/* global React */


var Loading =
/*#__PURE__*/
function (_React$Component4) {
  _inherits(Loading, _React$Component4);

  function Loading() {
    _classCallCheck(this, Loading);

    return _possibleConstructorReturn(this, _getPrototypeOf(Loading).apply(this, arguments));
  }

  _createClass(Loading, [{
    key: "render",
    value: function render() {
      return React.createElement("div", {
        className: "modal is-active"
      }, React.createElement("div", {
        className: "modal-background"
      }), React.createElement("div", {
        className: "modal-content has-text-centered animated rotateIn infinite"
      }, React.createElement("span", {
        className: "icon is-large has-text-danger"
      }, React.createElement("ion-icon", {
        name: "cog",
        class: "is-size-1"
      }))));
    }
  }]);

  return Loading;
}(React.Component);
/* global React, storage, getMaterialsByName */


var MainPage =
/*#__PURE__*/
function (_React$Component5) {
  _inherits(MainPage, _React$Component5);

  function MainPage(props) {
    var _this3;

    _classCallCheck(this, MainPage);

    _this3 = _possibleConstructorReturn(this, _getPrototypeOf(MainPage).call(this, props));
    _this3.state = {
      'showLoading': false,
      'showError': false,
      'showResults': false,
      'orderBy': 'year'
    };
    _this3.orderBy = {
      'year': function year(a, b) {
        if (a.year === null) {
          return 1;
        }

        if (b.year === null) {
          return -1;
        }

        return b.year - a.year;
      },
      'title': function title(a, b) {
        if (a.title === null) {
          return 1;
        }

        if (b.title === null) {
          return -1;
        }

        return a.title.localeCompare(b.title);
      },
      'imdb': function imdb(a, b) {
        if (a.imdb.rating === null) {
          return 1;
        }

        if (b.imdb.rating === null) {
          return -1;
        }

        return b.imdb.rating - a.imdb.rating;
      },
      'kinopoisk': function kinopoisk(a, b) {
        if (a.kinopoisk.rating === null) {
          return 1;
        }

        if (b.kinopoisk.rating === null) {
          return -1;
        }

        return b.kinopoisk.rating - a.kinopoisk.rating;
      }
    };
    _this3.onFormSubmit = _this3.onFormSubmit.bind(_assertThisInitialized(_this3));
    _this3.onInputChange = _this3.onInputChange.bind(_assertThisInitialized(_this3));
    return _this3;
  }

  _createClass(MainPage, [{
    key: "onFormSubmit",
    value: function onFormSubmit(event) {
      event.preventDefault();
      getMaterialsByName(this, storage.query);
    }
  }, {
    key: "onInputChange",
    value: function onInputChange(event) {
      storage.query = event.target.value;
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      return React.createElement(React.Fragment, null, React.createElement("p", {
        className: "title is-1 is-spaced is-uppercase has-text-centered has-text-danger animated lightSpeedIn"
      }, "\u041A\u0438\u043D\u043E\u0442\u0435\u043A\u0430"), React.createElement("form", {
        onSubmit: this.onFormSubmit,
        className: "animated fadeInUp slow"
      }, React.createElement("div", {
        className: "field"
      }, React.createElement("div", {
        className: "control ".concat(this.state.showLoading === true && 'is-loading is-large', " has-icons-left")
      }, React.createElement("input", {
        required: "required",
        autofocus: "autofocus",
        className: "input is-radiusless is-large is-danger has-background-black",
        onChange: this.onInputChange
      }), React.createElement("span", {
        className: "icon is-small is-left animated fadeIn faster delay-1s"
      }, React.createElement("ion-icon", {
        name: "search"
      })))), React.createElement("button", {
        type: "submit",
        className: "is-hidden"
      })), React.createElement("br", null), React.createElement("div", {
        className: this.state.showError === false && 'is-hidden'
      }, React.createElement("div", {
        className: "notification is-danger is-radiusless"
      }, React.createElement("p", null, storage.error))), React.createElement("div", {
        className: this.state.showResults === false && 'is-hidden'
      }, React.createElement("div", {
        className: "tabs is-small is-right animated slideInDown"
      }, React.createElement("ul", null, React.createElement("li", {
        onClick: function onClick() {
          return _this4.setState({
            'orderBy': 'year'
          });
        },
        className: this.state.orderBy === 'year' && 'is-active'
      }, React.createElement("a", null, "\u043F\u043E \u0433\u043E\u0434\u0443")), React.createElement("li", {
        onClick: function onClick() {
          return _this4.setState({
            'orderBy': 'title'
          });
        },
        className: this.state.orderBy === 'title' && 'is-active'
      }, React.createElement("a", null, "\u043F\u043E \u043D\u0430\u0437\u0432\u0430\u043D\u0438\u044E")), React.createElement("li", {
        onClick: function onClick() {
          return _this4.setState({
            'orderBy': 'imdb'
          });
        },
        className: this.state.orderBy === 'imdb' && 'is-active'
      }, React.createElement("a", null, "\u043F\u043E \u0440\u0435\u0439\u0442\u0438\u043D\u0433\u0443 IMDb")), React.createElement("li", {
        onClick: function onClick() {
          return _this4.setState({
            'orderBy': 'kinopoisk'
          });
        },
        className: this.state.orderBy === 'kinopoisk' && 'is-active'
      }, React.createElement("a", null, "\u043F\u043E \u0440\u0435\u0439\u0442\u0438\u043D\u0433\u0443 \u041A\u0438\u043D\u043E\u041F\u043E\u0438\u0441\u043A")))), React.createElement("div", {
        className: "columns is-mobile is-multiline"
      }, this.state.showResults === true && storage.materials.sort(this.orderBy[this.state.orderBy]).map(function (material) {
        return React.createElement(Material, {
          material: material
        });
      }))));
    }
  }]);

  return MainPage;
}(React.Component);
/* global React */


var Material =
/*#__PURE__*/
function (_React$Component6) {
  _inherits(Material, _React$Component6);

  function Material() {
    _classCallCheck(this, Material);

    return _possibleConstructorReturn(this, _getPrototypeOf(Material).apply(this, arguments));
  }

  _createClass(Material, [{
    key: "render",
    value: function render() {
      return React.createElement("div", {
        key: this.props.material.id,
        className: "column is-6-mobile is-3-tablet is-3-desktop is-2-widescreen is-2-fullhd"
      }, React.createElement("div", {
        className: "card"
      }, React.createElement(ReactRouterDOM.Link, {
        target: "_blank",
        to: "/query/".concat(this.props.material.id, "/"),
        className: "card-image has-hover-toggle"
      }, React.createElement("figure", {
        className: "image is-2by3"
      }, React.createElement("img", {
        src: "https://st.kp.yandex.net/images/film_iphone/iphone360_".concat(this.props.material.id, ".jpg")
      })), React.createElement("div", {
        className: "is-overlay is-toggled is-flex is-flex-centered modal-background"
      }, React.createElement("span", {
        className: "icon is-large has-text-danger"
      }, React.createElement("ion-icon", {
        name: "play",
        class: "is-size-1"
      }))), React.createElement("div", {
        className: "is-overlay is-toggled is-flex is-flex-bottom"
      }, React.createElement("div", {
        className: "level is-mobile"
      }, React.createElement("div", {
        className: "level-item has-text-centered"
      }, React.createElement("div", null, React.createElement("p", {
        className: "heading has-text-danger"
      }, "IMDb"), React.createElement("p", {
        className: "title is-6"
      }, this.props.material.imdb.rating === null ? 'Н/Д' : this.props.material.imdb.rating.toFixed(2)))), React.createElement("div", {
        className: "level-item has-text-centered"
      }, React.createElement("div", null, React.createElement("p", {
        className: "heading has-text-danger"
      }, "\u041A\u0438\u043D\u043E\u041F\u043E\u0438\u0441\u043A"), React.createElement("p", {
        className: "title is-6"
      }, this.props.material.kinopoisk.rating === null ? 'Н/Д' : this.props.material.kinopoisk.rating.toFixed(2))))), React.createElement("br", null)), React.createElement("div", {
        className: "is-overlay"
      }, React.createElement("span", {
        className: "tag is-radiusless is-dark is-absolute"
      }, this.props.material.year)))), React.createElement("p", {
        className: "is-size-7 has-text-grey-light has-text-centered"
      }, this.props.material.title));
    }
  }]);

  return Material;
}(React.Component);
/* global React, storage, getMaterialById */


var MaterialPage =
/*#__PURE__*/
function (_React$Component7) {
  _inherits(MaterialPage, _React$Component7);

  function MaterialPage(props) {
    var _this5;

    _classCallCheck(this, MaterialPage);

    _this5 = _possibleConstructorReturn(this, _getPrototypeOf(MaterialPage).call(this, props));
    _this5.state = {
      'showLoading': storage.material === null,
      'showError': false
    };
    return _this5;
  }

  _createClass(MaterialPage, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (storage.material === null) {
        getMaterialById(this, this.props.match.params.query);
      }
    }
  }, {
    key: "renderValues",
    value: function renderValues(title, data) {
      if (data === null) {
        return '';
      }

      return React.createElement(React.Fragment, null, React.createElement("div", {
        className: "column is-2"
      }, React.createElement("p", {
        className: "has-text-weight-bold"
      }, title)), React.createElement("div", {
        className: "column is-10"
      }, React.createElement("div", {
        className: "tags"
      }, data.map(function (value) {
        return React.createElement("span", {
          className: "tag is-light"
        }, value);
      }))));
    }
  }, {
    key: "render",
    value: function render() {
      if (this.state.showLoading === true) {
        return React.createElement(Loading, null);
      }

      if (this.state.showError === true) {
        return React.createElement(Error, {
          error: storage.error
        });
      }

      return React.createElement("div", {
        className: "notification is-dark animated jackInTheBox"
      }, React.createElement("div", {
        className: "columns is-centered is-mobile is-multiline"
      }, React.createElement("div", {
        className: "column is-12-mobile is-9-tablet is-9-desktop is-10-widescreen is-10-fullhd"
      }, React.createElement("p", {
        className: "title"
      }, React.createElement("span", {
        className: "is-uppercase"
      }, storage.material.title), React.createElement("span", null, "\xA0"), React.createElement("sup", null, React.createElement("span", {
        className: "tag is-danger"
      }, storage.material.year))), React.createElement("p", {
        className: "subtitle"
      }, storage.material.tagline), React.createElement("div", {
        className: "buttons"
      }, React.createElement(ReactRouterDOM.NavLink, {
        strict: true,
        sensitive: true,
        className: "button is-danger is-rounded",
        to: "".concat(this.props.match.url, "online/")
      }, React.createElement("span", {
        className: "icon is-small"
      }, React.createElement("ion-icon", {
        name: "play"
      })), React.createElement("span", null, "\u0421\u043C\u043E\u0442\u0440\u0435\u0442\u044C \u043E\u043D\u043B\u0430\u0439\u043D")), React.createElement(ReactRouterDOM.NavLink, {
        strict: true,
        sensitive: true,
        className: "button is-white is-rounded is-outlined",
        to: "".concat(this.props.match.url, "trailer/")
      }, React.createElement("span", {
        className: "icon is-small"
      }, React.createElement("ion-icon", {
        name: "play"
      })), React.createElement("span", null, "\u0421\u043C\u043E\u0442\u0440\u0435\u0442\u044C \u0442\u0440\u0435\u0439\u043B\u0435\u0440")), React.createElement(ReactRouterDOM.NavLink, {
        strict: true,
        sensitive: true,
        className: "button is-white is-rounded is-outlined is-static",
        to: "".concat(this.props.match.url, "torrents/")
      }, React.createElement("span", {
        className: "icon is-small"
      }, React.createElement("ion-icon", {
        name: "download"
      })), React.createElement("span", null, "\u0421\u043A\u0430\u0447\u0430\u0442\u044C \u0442\u043E\u0440\u0440\u0435\u043D\u0442"))), React.createElement("div", {
        className: "content"
      }, React.createElement("p", null, storage.material.description)), React.createElement("div", {
        className: "columns is-centered is-mobile is-multiline"
      }, this.renderValues('Жанры', storage.material.genres), this.renderValues('Актёры', storage.material.actors), this.renderValues('Режиссёры', storage.material.directors), this.renderValues('Студии', storage.material.studios))), React.createElement("div", {
        className: "column is-hidden-mobile is-3-tablet is-3-desktop is-2-widescreen is-2-fullhd"
      }, React.createElement("figure", {
        className: "image is-2by3"
      }, React.createElement("img", {
        src: "https://st.kp.yandex.net/images/film_iphone/iphone360_".concat(this.props.match.params.query, ".jpg")
      })), React.createElement("br", null), React.createElement("div", {
        className: "level is-mobile"
      }, React.createElement("div", {
        className: "level-item has-text-centered"
      }, React.createElement("div", null, React.createElement("p", {
        className: "is-size-4 has-text-danger"
      }, storage.material.imdb.rating === null ? 'Н/Д' : storage.material.imdb.rating.toFixed(2)), React.createElement("p", {
        className: "heading"
      }, "IMDb"))), React.createElement("div", {
        className: "level-item has-text-centered"
      }, React.createElement("div", null, React.createElement("p", {
        className: "is-size-4 has-text-danger"
      }, storage.material.kinopoisk.rating === null ? 'Н/Д' : storage.material.kinopoisk.rating.toFixed(2)), React.createElement("p", {
        className: "heading"
      }, "\u041A\u0438\u043D\u043E\u041F\u043E\u0438\u0441\u043A")))))));
    }
  }]);

  return MaterialPage;
}(React.Component);
/* global React */


var Menu =
/*#__PURE__*/
function (_React$Component8) {
  _inherits(Menu, _React$Component8);

  function Menu() {
    _classCallCheck(this, Menu);

    return _possibleConstructorReturn(this, _getPrototypeOf(Menu).apply(this, arguments));
  }

  _createClass(Menu, [{
    key: "render",
    value: function render() {
      return React.createElement("div", {
        className: this.props.className
      }, React.createElement("p", {
        className: "subtitle is-uppercase has-text-centered"
      }, this.props.title), React.createElement("div", {
        className: "field"
      }, React.createElement("div", {
        className: "control has-icons-right"
      }, React.createElement("input", {
        className: "input is-small is-danger has-background-black",
        onChange: this.props.function
      }), React.createElement("span", {
        className: "icon is-small is-right"
      }, React.createElement("ion-icon", {
        name: "search"
      })))), React.createElement("div", {
        className: "menu"
      }, React.createElement("ul", {
        className: "menu-list"
      }, this.props.children)));
    }
  }]);

  return Menu;
}(React.Component);
/* global React, storage, getSourcesById */


var OnlinePage =
/*#__PURE__*/
function (_React$Component9) {
  _inherits(OnlinePage, _React$Component9);

  function OnlinePage(props) {
    var _this6;

    _classCallCheck(this, OnlinePage);

    _this6 = _possibleConstructorReturn(this, _getPrototypeOf(OnlinePage).call(this, props));
    _this6.state = {
      'showLoading': storage.sources === null,
      'showError': false
    };
    _this6.renderTranslators = _this6.renderTranslators.bind(_assertThisInitialized(_this6));
    _this6.renderSeasons = _this6.renderSeasons.bind(_assertThisInitialized(_this6));
    _this6.renderEpisodes = _this6.renderEpisodes.bind(_assertThisInitialized(_this6));
    _this6.renderPlayer = _this6.renderPlayer.bind(_assertThisInitialized(_this6));
    _this6.index = {
      'source': null,
      'translator': null,
      'season': null,
      'episode': null
    };
    return _this6;
  }

  _createClass(OnlinePage, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (storage.sources === null) {
        getSourcesById(this, this.props.match.params.query);
      }
    }
  }, {
    key: "renderTranslators",
    value: function renderTranslators(props) {
      this.index.source = parseInt(props.match.params.source_index);
      var translators = storage.sources[this.index.source].translators;
      return React.createElement(Translators, {
        pathname: props.match.url,
        translators: translators
      });
    }
  }, {
    key: "renderSeasons",
    value: function renderSeasons(props) {
      this.index.translator = parseInt(props.match.params.translator_index);
      var seasons = storage.sources[this.index.source].translators[this.index.translator].seasons;
      return React.createElement(Seasons, {
        pathname: props.match.url,
        seasons: seasons
      });
    }
  }, {
    key: "renderEpisodes",
    value: function renderEpisodes(props) {
      this.index.season = parseInt(props.match.params.season_index);
      var episodes = storage.sources[this.index.source].translators[this.index.translator].seasons[this.index.season].episodes;
      return React.createElement(Episodes, {
        pathname: props.match.url,
        episodes: episodes
      });
    }
  }, {
    key: "renderPlayer",
    value: function renderPlayer(props) {
      var query;

      if (storage.hasEpisodes === true) {
        this.index.episode = parseInt(props.match.params.episode_index);
        query = storage.sources[this.index.source].translators[this.index.translator].seasons[this.index.season].episodes[this.index.episode].query;
      } else {
        this.index.translator = parseInt(props.match.params.translator_index);
        query = storage.sources[this.index.source].translators[this.index.translator].query;
      }

      var translator = storage.sources[this.index.source].translators[this.index.translator];
      return React.createElement(Player, {
        hostname: translator.hostname,
        pathname: translator.pathname,
        query: query
      });
    }
  }, {
    key: "render",
    value: function render() {
      if (this.state.showLoading === true) {
        return React.createElement(Loading, null);
      }

      if (this.state.showError === true) {
        return React.createElement(Error, {
          error: storage.error
        });
      }

      var additionalElements = '';
      var additionalPath = '';

      if (storage.hasEpisodes === true) {
        additionalElements = React.createElement(React.Fragment, null, React.createElement(ReactRouterDOM.Route, {
          strict: true,
          sensitive: true,
          path: "/query/:query/online/:source_index/:translator_index/",
          render: this.renderSeasons
        }), React.createElement(ReactRouterDOM.Route, {
          strict: true,
          sensitive: true,
          path: "/query/:query/online/:source_index/:translator_index/:season_index/",
          render: this.renderEpisodes
        }));
        additionalPath = ':season_index/:episode_index/';
      }

      return React.createElement(React.Fragment, null, React.createElement("div", {
        className: "section"
      }, React.createElement("div", {
        className: "columns is-mobile"
      }, React.createElement(Sources, {
        pathname: this.props.match.url,
        sources: storage.sources
      }), React.createElement(ReactRouterDOM.Route, {
        strict: true,
        sensitive: true,
        path: "/query/:query/online/:source_index/",
        render: this.renderTranslators
      }), additionalElements)), React.createElement(ReactRouterDOM.Route, {
        strict: true,
        sensitive: true,
        path: "/query/:query/online/:source_index/:translator_index/".concat(additionalPath),
        render: this.renderPlayer
      }));
    }
  }]);

  return OnlinePage;
}(React.Component);
/* global React */


var Player =
/*#__PURE__*/
function (_React$Component10) {
  _inherits(Player, _React$Component10);

  function Player(props) {
    var _this7;

    _classCallCheck(this, Player);

    _this7 = _possibleConstructorReturn(this, _getPrototypeOf(Player).call(this, props));
    _this7.query = props.query === null ? '' : new URLSearchParams(Object.entries(props.query).map(function (array) {
      return array.join('=');
    }).join('&'));
    _this7.close = document.location.pathname.slice(0, -1);
    _this7.close = _this7.close.slice(0, _this7.close.lastIndexOf('/') + 1);
    return _this7;
  }

  _createClass(Player, [{
    key: "render",
    value: function render() {
      return React.createElement("div", {
        className: "modal is-active"
      }, React.createElement("div", {
        className: "modal-background"
      }), React.createElement("div", {
        className: "modal-content"
      }, React.createElement("div", {
        className: "image is-16by9"
      }, React.createElement("iframe", {
        allowfullscreen: "allowfullscreen",
        className: "is-block",
        src: "https://".concat(this.props.hostname).concat(this.props.pathname, "?").concat(this.query)
      }))), React.createElement(ReactRouterDOM.NavLink, {
        strict: true,
        sensitive: true,
        className: "modal-close is-large",
        to: this.close
      }));
    }
  }]);

  return Player;
}(React.Component);
/* global React */


var Season =
/*#__PURE__*/
function (_React$Component11) {
  _inherits(Season, _React$Component11);

  function Season() {
    _classCallCheck(this, Season);

    return _possibleConstructorReturn(this, _getPrototypeOf(Season).apply(this, arguments));
  }

  _createClass(Season, [{
    key: "render",
    value: function render() {
      return React.createElement("li", null, React.createElement(ReactRouterDOM.NavLink, {
        strict: true,
        sensitive: true,
        activeClassName: "is-active",
        to: this.props.to
      }, React.createElement("p", {
        className: "subtitle"
      }, this.props.season.season, " \u0441\u0435\u0437\u043E\u043D"), React.createElement("div", {
        className: "tags"
      }, React.createElement("span", {
        className: "tag is-dark"
      }, "\u0421\u0435\u0440\u0438\u0439: ", this.props.season.episodes.length))));
    }
  }]);

  return Season;
}(React.Component);
/* global React */


var Seasons =
/*#__PURE__*/
function (_React$Component12) {
  _inherits(Seasons, _React$Component12);

  function Seasons(props) {
    var _this8;

    _classCallCheck(this, Seasons);

    _this8 = _possibleConstructorReturn(this, _getPrototypeOf(Seasons).call(this, props));
    _this8.state = {
      'query': -1
    };
    _this8.onInputChange = _this8.onInputChange.bind(_assertThisInitialized(_this8));
    return _this8;
  }

  _createClass(Seasons, [{
    key: "onInputChange",
    value: function onInputChange(event) {
      var query = parseInt(event.target.value.trim());
      this.setState({
        'query': Number.isNaN(query) === true ? -1 : query
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this9 = this;

      return React.createElement(Menu, {
        className: "column is-2 animated slideInDown",
        title: "\u0421\u0435\u0437\u043E\u043D\u044B",
        function: this.onInputChange
      }, this.props.seasons.map(function (season, seasonIndex) {
        return season.season >= _this9.state.query && React.createElement(Season, {
          to: "".concat(_this9.props.pathname).concat(seasonIndex, "/"),
          season: season
        });
      }));
    }
  }]);

  return Seasons;
}(React.Component);
/* global React */


var Source =
/*#__PURE__*/
function (_React$Component13) {
  _inherits(Source, _React$Component13);

  function Source() {
    _classCallCheck(this, Source);

    return _possibleConstructorReturn(this, _getPrototypeOf(Source).apply(this, arguments));
  }

  _createClass(Source, [{
    key: "render",
    value: function render() {
      return React.createElement("li", null, React.createElement(ReactRouterDOM.NavLink, {
        strict: true,
        sensitive: true,
        activeClassName: "is-active",
        to: this.props.to
      }, React.createElement("p", {
        className: "subtitle"
      }, this.props.source.source)));
    }
  }]);

  return Source;
}(React.Component);
/* global React, storage */


var Sources =
/*#__PURE__*/
function (_React$Component14) {
  _inherits(Sources, _React$Component14);

  function Sources(props) {
    var _this10;

    _classCallCheck(this, Sources);

    _this10 = _possibleConstructorReturn(this, _getPrototypeOf(Sources).call(this, props));
    _this10.state = {
      'query': ''
    };
    _this10.onInputChange = _this10.onInputChange.bind(_assertThisInitialized(_this10));
    return _this10;
  }

  _createClass(Sources, [{
    key: "onInputChange",
    value: function onInputChange(event) {
      this.setState({
        'query': event.target.value.trim().toLowerCase()
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this11 = this;

      return React.createElement(Menu, {
        className: "column ".concat(storage.hasEpisodes === true ? 'is-3' : 'is-6', " animated slideInDown"),
        title: "\u0418\u0441\u0442\u043E\u0447\u043D\u0438\u043A\u0438",
        function: this.onInputChange
      }, this.props.sources.map(function (source, sourceIndex) {
        return source.source.startsWith(_this11.state.query) === true && React.createElement(Source, {
          to: "".concat(_this11.props.pathname).concat(sourceIndex, "/"),
          source: source
        });
      }));
    }
  }]);

  return Sources;
}(React.Component);
/* global React, storage, getMaterialById, getYouTubeTrailersByName */


var TrailersPage =
/*#__PURE__*/
function (_React$Component15) {
  _inherits(TrailersPage, _React$Component15);

  function TrailersPage(props) {
    var _this12;

    _classCallCheck(this, TrailersPage);

    _this12 = _possibleConstructorReturn(this, _getPrototypeOf(TrailersPage).call(this, props));
    _this12.state = {
      'showLoading': storage.trailers === null,
      'showError': false
    };
    return _this12;
  }

  _createClass(TrailersPage, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this13 = this;

      if (storage.trailers === null) {
        if (storage.material === null) {
          getMaterialById(this, this.props.match.params.query, true).then(function () {
            return getYouTubeTrailersByName(_this13, storage.material.title);
          });
        } else {
          getYouTubeTrailersByName(this, storage.material.title);
        }
      }
    }
  }, {
    key: "renderPlayer",
    value: function renderPlayer(props) {
      var trailer = storage.trailers[props.match.params.trailer_index];
      return React.createElement(Player, {
        hostname: trailer.hostname,
        pathname: trailer.pathname,
        query: trailer.query
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this14 = this;

      if (this.state.showLoading === true) {
        return React.createElement(Loading, null);
      }

      if (this.state.showError === true) {
        return React.createElement(Error, {
          error: storage.error
        });
      }

      return React.createElement(React.Fragment, null, React.createElement("div", {
        className: "menu"
      }, React.createElement("ul", {
        className: "menu-list"
      }, storage.trailers.map(function (trailer, trailerIndex) {
        return React.createElement("li", null, React.createElement(ReactRouterDOM.NavLink, {
          strict: true,
          sensitive: true,
          exact: true,
          activeClassName: "is-active",
          to: "".concat(_this14.props.match.url).concat(trailerIndex, "/")
        }, React.createElement("div", {
          className: "columns"
        }, React.createElement("div", {
          className: "column is-2"
        }, React.createElement("figure", {
          className: "image is-4by3"
        }, React.createElement("img", {
          src: "https://i.ytimg.com/vi/".concat(trailer.id, "/hqdefault.jpg")
        }))), React.createElement("div", {
          className: "column is-10"
        }, React.createElement("p", {
          className: "is-size-6"
        }, trailer.title), React.createElement("br", null), React.createElement("p", {
          className: "is-size-6"
        }, React.createElement("span", {
          className: "tag is-dark"
        }, trailer.channel))))));
      }))), React.createElement(ReactRouterDOM.Route, {
        strict: true,
        sensitive: true,
        exact: true,
        path: "/query/:query/trailer/:trailer_index/",
        render: this.renderPlayer
      }));
    }
  }]);

  return TrailersPage;
}(React.Component);
/* global React, storage */


var Translator =
/*#__PURE__*/
function (_React$Component16) {
  _inherits(Translator, _React$Component16);

  function Translator() {
    _classCallCheck(this, Translator);

    return _possibleConstructorReturn(this, _getPrototypeOf(Translator).apply(this, arguments));
  }

  _createClass(Translator, [{
    key: "render",
    value: function render() {
      var tags = [];

      if (storage.hasEpisodes === true) {
        tags.push("\u0421\u0435\u0437\u043E\u043D\u043E\u0432: ".concat(this.props.translator.seasons.length));
      } else {
        switch (this.props.translator.has_ads) {
          case true:
            tags.push('Есть реклама');
            break;

          case false:
            tags.push('Без рекламы');
            break;
        }

        switch (this.props.translator.is_camrip) {
          case true:
            tags.push('Экранка');
            break;
        }

        switch (this.props.translator.is_extended) {
          case true:
            tags.push('Расширенная версия');
            break;
        }

        switch (this.props.translator.quality) {
          case null:
            break;

          default:
            tags.push(this.props.translator.quality);
        }
      }

      return React.createElement("li", null, React.createElement(ReactRouterDOM.NavLink, {
        strict: true,
        sensitive: true,
        activeClassName: "is-active",
        to: this.props.to
      }, React.createElement("p", {
        className: "subtitle"
      }, this.props.translator.translator), React.createElement("div", {
        className: "tags"
      }, tags.map(function (tag) {
        return React.createElement("span", {
          className: "tag is-dark"
        }, tag);
      }))));
    }
  }]);

  return Translator;
}(React.Component);
/* global React, storage */


var Translators =
/*#__PURE__*/
function (_React$Component17) {
  _inherits(Translators, _React$Component17);

  function Translators(props) {
    var _this15;

    _classCallCheck(this, Translators);

    _this15 = _possibleConstructorReturn(this, _getPrototypeOf(Translators).call(this, props));
    _this15.state = {
      'query': ''
    };
    _this15.onInputChange = _this15.onInputChange.bind(_assertThisInitialized(_this15));
    return _this15;
  }

  _createClass(Translators, [{
    key: "onInputChange",
    value: function onInputChange(event) {
      this.setState({
        'query': event.target.value.trim().toLocaleLowerCase()
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this16 = this;

      return React.createElement(Menu, {
        className: "column ".concat(storage.hasEpisodes === true ? 'is-5' : 'is-6', " animated slideInDown"),
        title: "\u041E\u0437\u0432\u0443\u0447\u043A\u0438",
        function: this.onInputChange
      }, this.props.translators.map(function (translator, translatorIndex) {
        return translator.translator.toLocaleLowerCase().startsWith(_this16.state.query) === true && React.createElement(Translator, {
          to: "".concat(_this16.props.pathname).concat(translatorIndex, "/"),
          translator: translator
        });
      }));
    }
  }]);

  return Translators;
}(React.Component);