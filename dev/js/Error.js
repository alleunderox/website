/* global React */
class Error extends React.Component {
  render () {
    return (
      <div className='modal is-active'>
        <div className='modal-background' />
        <div className='modal-content animated bounce'>
          <div className='notification is-danger'>
            <p className='title'>Упс...</p>
            <p>{this.props.error.text}</p>
          </div>
        </div>
      </div>
    )
  }
}
