/* global React, storage */
class Translators extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      'query': ''
    }
    this.onInputChange = this.onInputChange.bind(this)
  }

  onInputChange (event) {
    this.setState({
      'query': event.target.value.trim().toLocaleLowerCase()
    })
  }

  render () {
    return (
      <Menu className={`column ${storage.hasEpisodes === true ? 'is-5' : 'is-6'} animated slideInDown`} title='Озвучки' function={this.onInputChange}>
        {
          this
            .props
            .translators
            .map((translator, translatorIndex) => translator
              .translator
              .toLocaleLowerCase()
              .startsWith(this.state.query) === true && <Translator to={`${this.props.pathname}${translatorIndex}/`} translator={translator} />)
        }
      </Menu>
    )
  }
}
