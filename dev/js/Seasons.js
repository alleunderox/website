/* global React */
class Seasons extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      'query': -1
    }
    this.onInputChange = this.onInputChange.bind(this)
  }

  onInputChange (event) {
    const query = parseInt(event.target.value.trim())
    this.setState({
      'query': Number.isNaN(query) === true ? -1 : query
    })
  }

  render () {
    return (
      <Menu className='column is-2 animated slideInDown' title='Сезоны' function={this.onInputChange}>
        {
          this
            .props
            .seasons
            .map((season, seasonIndex) => season
              .season >= this.state.query && <Season to={`${this.props.pathname}${seasonIndex}/`} season={season} />)
        }
      </Menu>
    )
  }
}
