/* global React */
class Episodes extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      'query': -1
    }
    this.onInputChange = this.onInputChange.bind(this)
  }

  onInputChange (event) {
    const query = parseInt(event.target.value.trim())
    this.setState({
      'query': Number.isNaN(query) === true ? -1 : query
    })
  }

  render () {
    return (
      <Menu className='column is-2 animated slideInDown' title='Серии' function={this.onInputChange}>
        {
          this
            .props
            .episodes
            .map((episode, episodeIndex) => episode
              .episode >= this.state.query && <Episode to={`${this.props.pathname}${episodeIndex}/`} episode={episode} />)
        }
      </Menu>
    )
  }
}
