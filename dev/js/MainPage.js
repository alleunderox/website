/* global React, storage, getMaterialsByName */
class MainPage extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      'showLoading': false,
      'showError': false,
      'showResults': false,
      'orderBy': 'year'
    }
    this.orderBy = {
      'year': (a, b) => {
        if (a.year === null) {
          return 1
        }
        if (b.year === null) {
          return -1
        }
        return b.year - a.year
      },
      'title': (a, b) => {
        if (a.title === null) {
          return 1
        }
        if (b.title === null) {
          return -1
        }
        return a.title.localeCompare(b.title)
      },
      'imdb': (a, b) => {
        if (a.imdb.rating === null) {
          return 1
        }
        if (b.imdb.rating === null) {
          return -1
        }
        return b.imdb.rating - a.imdb.rating
      },
      'kinopoisk': (a, b) => {
        if (a.kinopoisk.rating === null) {
          return 1
        }
        if (b.kinopoisk.rating === null) {
          return -1
        }
        return b.kinopoisk.rating - a.kinopoisk.rating
      }
    }
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onInputChange = this.onInputChange.bind(this)
  }

  onFormSubmit (event) {
    event.preventDefault()
    getMaterialsByName(this, storage.query)
  }

  onInputChange (event) {
    storage.query = event.target.value
  }

  render () {
    return (
      <React.Fragment>
        <p className='title is-1 is-spaced is-uppercase has-text-centered has-text-danger animated lightSpeedIn'>Кинотека</p>
        <form onSubmit={this.onFormSubmit} className='animated fadeInUp slow'>
          <div className='field'>
            <div className={`control ${this.state.showLoading === true && 'is-loading is-large'} has-icons-left`}>
              <input required='required' autofocus='autofocus' className='input is-radiusless is-large is-danger has-background-black' onChange={this.onInputChange} />
              <span className='icon is-small is-left animated fadeIn faster delay-1s'>
                <ion-icon name='search' />
              </span>
            </div>
          </div>
          <button type='submit' className='is-hidden' />
        </form>
        <br />
        <div className={this.state.showError === false && 'is-hidden'}>
          <div className='notification is-danger is-radiusless'>
            <p>{storage.error}</p>
          </div>
        </div>
        <div className={this.state.showResults === false && 'is-hidden'}>
          <div className='tabs is-small is-right animated slideInDown'>
            <ul>
              <li onClick={() => this.setState({ 'orderBy': 'year' })} className={this.state.orderBy === 'year' && 'is-active'}><a>по году</a></li>
              <li onClick={() => this.setState({ 'orderBy': 'title' })} className={this.state.orderBy === 'title' && 'is-active'}><a>по названию</a></li>
              <li onClick={() => this.setState({ 'orderBy': 'imdb' })} className={this.state.orderBy === 'imdb' && 'is-active'}><a>по рейтингу IMDb</a></li>
              <li onClick={() => this.setState({ 'orderBy': 'kinopoisk' })} className={this.state.orderBy === 'kinopoisk' && 'is-active'}><a>по рейтингу КиноПоиск</a></li>
            </ul>
          </div>
          <div className='columns is-mobile is-multiline'>
            {
              this.state.showResults === true && storage
                .materials
                .sort(this.orderBy[this.state.orderBy])
                .map(material => <Material material={material} />)
            }
          </div>
        </div>
      </React.Fragment>
    )
  }
}
