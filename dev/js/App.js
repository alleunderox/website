/* global ReactDOM, fetch, Headers, Request, MainPage, MaterialPage, OnlinePage, TrailersPage */
'use strict'

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render((
    <ReactRouterDOM.BrowserRouter>
      <div className='hero is-black is-bold is-fullheight'>
        <div className='hero-head'>
          <div className='container'>
            <ReactRouterDOM.Route strict sensitive path='/query/:query/online/' component={OnlinePage} />
          </div>
        </div>
        <div className='hero-body'>
          <div className='container'>
            <ReactRouterDOM.Route strict sensitive exact path='/' component={MainPage} />
            <ReactRouterDOM.Route strict sensitive exact path='/query/:query/' component={MaterialPage} />
            <ReactRouterDOM.Route strict sensitive exact path='/query/:query/trailer/' component={TrailersPage} />
          </div>
        </div>
      </div>
    </ReactRouterDOM.BrowserRouter>
  ), document.getElementById('react-dom'))
})

const storage = {
  'material': null,
  'materials': null,
  'trailers': null,
  'sources': null,
  'error': null,
  'hasEpisodes': null,
  'query': null
}

const getMaterialsByName = (parent, query) => {
  parent.setState({
    'showLoading': true,
    'showError': false
  })
  const body = JSON.stringify({ query })
  const headers = new Headers({
    'Content-Type': 'application/json'
  })
  const request = new Request('/api/getMaterialsByName', {
    'method': 'POST',
    'cache': 'no-store',
    'body': body,
    'headers': headers
  })
  fetch(request)
    .then((response) => {
      if (response.ok === true) {
        return response.json()
      }
      return {
        'error': {
          'text': '...'
        }
      }
    })
    .then((data) => {
      if (data.hasOwnProperty('error') === true) {
        storage.error = data.error.text
        parent.setState({
          'showLoading': false,
          'showError': true,
          'showResults': false
        })
      } else {
        storage.materials = data.materials
        parent.setState({
          'showLoading': false,
          'showError': false,
          'showResults': true
        })
      }
    })
}

const getMaterialById = (parent, query, wait = false) => {
  const body = JSON.stringify({ query })
  const headers = new Headers({
    'Content-Type': 'application/json'
  })
  const request = new Request('/api/getMaterialById', {
    'method': 'POST',
    'cache': 'no-store',
    'body': body,
    'headers': headers
  })
  return fetch(request)
    .then((response) => {
      if (response.ok === true) {
        return response.json()
      }
      return {
        'error': {
          'text': '...'
        }
      }
    })
    .then((data) => {
      if (data.hasOwnProperty('error') === true) {
        storage.error = data.error
        parent.setState({
          'showLoading': false,
          'showError': true
        })
      } else {
        storage.material = data.material
        if (wait === false) {
          parent.setState({
            'showLoading': false,
            'showError': false
          })
        }
      }
    })
}

const getYouTubeTrailersByName = (parent, query) => {
  const body = JSON.stringify({ query })
  const headers = new Headers({
    'Content-Type': 'application/json'
  })
  const request = new Request('/api/getYouTubeTrailersByName', {
    'method': 'POST',
    'cache': 'no-store',
    'body': body,
    'headers': headers
  })
  return fetch(request)
    .then((response) => {
      if (response.ok === true) {
        return response.json()
      }
      return {
        'error': {
          'text': '...'
        }
      }
    })
    .then((data) => {
      if (data.hasOwnProperty('error') === true) {
        storage.error = data.error
        parent.setState({
          'showLoading': false,
          'showError': true
        })
      } else {
        storage.trailers = data.trailers
        parent.setState({
          'showLoading': false,
          'showError': false
        })
      }
    })
}

const getSourcesById = (parent, query) => {
  const body = JSON.stringify({ query })
  const headers = new Headers({
    'Content-Type': 'application/json'
  })
  const request = new Request('/api/getSourcesById', {
    'method': 'POST',
    'cache': 'no-store',
    'body': body,
    'headers': headers
  })
  return fetch(request)
    .then((response) => {
      if (response.ok === true) {
        return response.json()
      }
      return {
        'error': {
          'text': '...'
        }
      }
    })
    .then((data) => {
      if (data.hasOwnProperty('error') === true) {
        storage.error = data.error
        parent.setState({
          'showLoading': false,
          'showError': true
        })
      } else {
        storage.sources = data.sources
        storage.hasEpisodes = data.sources[0].translators[0].hasOwnProperty('seasons') === true
        parent.setState({
          'showLoading': false,
          'showError': false
        })
      }
    })
}
