/* global React */
class Episode extends React.Component {
  render () {
    return (
      <li>
        <ReactRouterDOM.NavLink strict sensitive activeClassName='is-active' to={this.props.to}>
          <p className='subtitle'>{this.props.episode.episode} серия</p>
        </ReactRouterDOM.NavLink>
      </li>
    )
  }
}
