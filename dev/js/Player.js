/* global React */
class Player extends React.Component {
  constructor (props) {
    super(props)
    this.query = props.query === null ? '' : new URLSearchParams(Object.entries(props.query).map(array => array.join('=')).join('&'))
    this.close = document.location.pathname.slice(0, -1)
    this.close = this.close.slice(0, this.close.lastIndexOf('/') + 1)
  }

  render () {
    return (
      <div className='modal is-active'>
        <div className='modal-background' />
        <div className='modal-content'>
          <div className='image is-16by9'>
            <iframe allowfullscreen='allowfullscreen' className='is-block' src={`https://${this.props.hostname}${this.props.pathname}?${this.query}`} />
          </div>
        </div>
        <ReactRouterDOM.NavLink strict sensitive className='modal-close is-large' to={this.close} />
      </div>
    )
  }
}
