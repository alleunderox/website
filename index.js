'use strict'

require('dotenv').config()

const path = require('path')
const express = require('express')
const compression = require('compression')
const helmet = require('helmet')
const make = require('request-promise')
const cors = require('cors')
const fs = require('fs-extra')
const morgan = require('morgan')

const app = express()

app.use(compression(), helmet(), morgan(':method :url :status :response-time'))
app.use('/api/', express.json(), cors())
app.use('/static/', express.static(path.resolve('static')))

app.get([
  '/',
  '/query/:query/',
  '/query/:query/trailer/',
  '/query/:query/trailer/:trailer_index/',
  '/query/:query/online/',
  '/query/:query/online/:source_index/',
  '/query/:query/online/:source_index/:translator_index/',
  '/query/:query/online/:source_index/:translator_index/:season_index/',
  '/query/:query/online/:source_index/:translator_index/:season_index/:episode_index/'
], (request, response) => response.sendFile(path.resolve('index.html')))

app.post('/api/getSourcesById', (request, response) => {
  if (request.body.hasOwnProperty('query') === false) {
    return response.json({
      'error': true
    })
  }
  const cache = `getSourcesById.${request.body.query}.json`
  fs
    .readJSON(path.resolve('cache', cache))
    .then(data => {
      if (Date.now() - data.timestamp > 600000) {
        return Promise.reject(new Error())
      }
      response.json(data)
    })
    .catch(() => Promise
      .all([
        make // http://docs.moonwalk.cc/docs/api-docs
          .get({
            'uri': 'https://streamguard.cc/api/videos.json',
            'gzip': true,
            'json': true,
            'timeout': 1000,
            'qs': {
              'kinopoisk_id': request.body.query,
              'api_token': process.env.MOONWALK_CC
            },
            'agentOptions': {
              'rejectUnauthorized': false
            }
          })
          .then(data => {
            if (Array.isArray(data) === false || data.length === 0) { // ERROR
              return null
            }
            if (data[0].hasOwnProperty('season_episodes_count') === true) {
              return data.map(translator => ({
                'hostname': 'streamguard.cc',
                'pathname': new URL(translator.iframe_url).pathname,
                'translator': translator.translator === null ? 'Неизвестно' : translator.translator,
                'seasons': translator.season_episodes_count.map(season => ({
                  'season': season.season_number,
                  'episodes': season.episodes.map(episode => ({
                    'episode': episode,
                    'query': {
                      'nocontrols': 1,
                      'season': season.season_number,
                      'episode': episode
                    }
                  }))
                }))
              }))
            }
            return data.map(translator => ({
              'hostname': 'streamguard.cc',
              'pathname': new URL(translator.iframe_url).pathname,
              'query': {
                'nocontrols': 1
              },
              'translator': translator.translator === null ? 'Неизвестно' : translator.translator,
              'quality': translator.source_type,
              'is_camrip': translator.camrip,
              'is_extended': translator.directors_version,
              'has_ads': translator.instream_ads
            }))
          })
          .catch(() => null),
        make // https://iframe.video/api_docs.php
          .get({
            'uri': 'https://iframe.video/api/v2/search',
            'gzip': true,
            'json': true,
            'timeout': 1000,
            'qs': {
              'kp': request.body.query,
              'include': 'media'
            },
            'agentOptions': {
              'rejectUnauthorized': false
            }
          })
          .then(data => {
            if (data.hasOwnProperty('total') === false || data.total === 0) { // ERROR
              return null
            }
            if (data.results[0].hasOwnProperty('seasons') === true) {
              const temp = {}
              data.results[0].seasons.forEach(translator => {
                const key = translator.translate_id.toString()
                if (temp.hasOwnProperty(key) === false) {
                  temp[key] = []
                }
                temp[key].push(translator)
              })
              return Object.values(temp).map(translator => ({
                'hostname': new URL(translator[0].path).hostname,
                'pathname': new URL(translator[0].path).pathname,
                'translator': translator[0].translate,
                'seasons': translator.map(season => ({
                  'season': season.season_num,
                  'episodes': season.episodes.map(episode => ({
                    'episode': episode,
                    'query': {
                      'noall': true,
                      'color': 'ff3860',
                      'skin': 'slim',
                      'season': season.season_num,
                      'episode': episode
                    }
                  }))
                }))
              }))
            }
            return data.results[0].versions.map(translator => ({
              'hostname': new URL(translator.path).hostname,
              'pathname': new URL(translator.path).pathname,
              'query': {
                'noall': true,
                'color': 'ff3860',
                'skin': 'slim'
              },
              'translator': translator.translate,
              'quality': translator.source === null ? null : `${translator.source}Rip`,
              'is_camrip': null,
              'is_extended': null,
              'has_ads': null
            }))
          })
          .catch(() => null),
        make // http://bd.kodik.biz/api/info
          .get({
            'uri': 'https://kodikapi.com/search',
            'gzip': true,
            'json': true,
            'timeout': 1000,
            'qs': {
              'kinopoisk_id': request.body.query,
              'token': process.env.KODIK_CC,
              'with_episodes': true
            },
            'agentOptions': {
              'rejectUnauthorized': false
            }
          })
          .then(data => {
            if (data.hasOwnProperty('total') === false || data.total === 0) { // ERROR
              return null
            }
            if (data.results[0].hasOwnProperty('seasons') === true) {
              return data.results.map(translator => ({
                'hostname': new URL(translator.link).hostname,
                'pathname': new URL(translator.link).pathname,
                'translator': translator.translation.title,
                'seasons': Object.entries(translator.seasons).map(season => ({
                  'season': season[0],
                  'episodes': Object.keys(season[1].episodes).map(episode => ({
                    'episode': episode,
                    'query': {
                      'translations': false,
                      'season': season[0],
                      'episode': episode
                    }
                  }))
                }))
              }))
            }
            return data.results.map(translator => ({
              'hostname': new URL(translator.link).hostname,
              'pathname': new URL(translator.link).pathname,
              'query': {
                'translations': false
              },
              'translator': translator.translation.title,
              'quality': translator.quality === null ? null : translator.quality.split(' ')[0],
              'is_camrip': translator.camrip,
              'is_extended': null,
              'has_ads': null
            }))
          })
          .catch(() => null),
        make // https://hdbaza.com/docs#api
          .get({
            'uri': 'https://hdbaza.com/api/movies',
            'gzip': true,
            'json': true,
            'timeout': 1000,
            'qs': {
              'kpid': request.body.query,
              'uh': process.env.HDBAZA_COM
            },
            'agentOptions': {
              'rejectUnauthorized': false
            }
          })
          .then(data => {
            if (data.hasOwnProperty('data') === false || Array.isArray(data.data) === false || data.data.length === 0) { // ERROR
              return null
            }
            if (data.data[0].is_multiseries === 1) {
              return data.data[0].sounds.map(translator => ({
                'hostname': new URL(data.data[0].iframe_url).hostname,
                'pathname': new URL(data.data[0].iframe_url).pathname,
                'translator': translator.name,
                'seasons': translator.seasons_info.map(season => ({
                  'season': season.season_number,
                  'episodes': season.episodes.map(episode => ({
                    'episode': episode,
                    'query': {
                      'uh': process.env.HDBAZA_COM,
                      'psid': '47aefe1d5af3f8b4',
                      'mh': data.data[0].hash,
                      'snd': translator.id,
                      's': season.season_number,
                      'e': episode
                    }
                  }))
                }))
              }))
            }
            return data.data[0].sounds.map(translator => ({
              'hostname': new URL(data.data[0].iframe_url).hostname,
              'pathname': new URL(data.data[0].iframe_url).pathname,
              'query': {
                'uh': process.env.HDBAZA_COM,
                'psid': '47aefe1d5af3f8b4',
                'mh': data.data[0].hash,
                'snd': translator.id
              },
              'translator': translator.name,
              'quality': data.data[0].source_type,
              'is_camrip': data.data[0].is_camrip === 1,
              'is_extended': null,
              'has_ads': null
            }))
          })
          .catch(() => null),
        make
          .get({
            'uri': 'https://vio.to/api/video.json',
            'gzip': true,
            'json': true,
            'timeout': 1000,
            'qs': {
              'kinopoisk_id': request.body.query,
              'token': process.env.VIO_TO
            },
            'agentOptions': {
              'rejectUnauthorized': false
            }
          })
          .then(data => {
            if (Array.isArray(data) === false || data.length === 0) { // ERROR
              return null
            }
            if (data[0].hasOwnProperty('seasons_count') === true) {
              return null
            }
            return data.map(translator => ({
              'hostname': new URL(translator.iframe_url).hostname,
              'pathname': new URL(translator.iframe_url).pathname,
              'query': null,
              'translator': translator.translator,
              'quality': translator.quality === null ? null : translator.quality.split(' ')[1],
              'is_camrip': null,
              'is_extended': null,
              'has_ads': null
            }))
          })
          .catch(() => null)
      ])
      .then(data => {
        const sources = [
          'streamguard.cc',
          'iframe.video',
          'kodik.biz',
          'hdbaza.com',
          'vio.to'
        ]
        const temp = {
          'sources': [],
          'timestamp': Date.now()
        }
        data.forEach((translators, index) => {
          if (translators !== null) {
            temp.sources.push({
              'source': sources[index],
              'translators': translators
            })
          }
        })
        if (temp.sources.length === 0) {
          return response.json({
            'error': {
              'text': 'По данному запросу ничего не нашлось.'
            }
          })
        }
        return fs
          .writeJSON(path.resolve('cache', cache), temp)
          .then(() => response.json(temp))
      })
      .catch(error => {
        response.status(500).end()
        console.error(error)
      })
    )
})

app.post('/api/getMaterialById', (request, response) => {
  if (request.body.hasOwnProperty('query') === false) {
    return response.json({
      'error': true
    })
  }
  const cache = `getMaterialById.${request.body.query}.json`
  fs
    .readJSON(path.resolve('cache', cache))
    .then(data => {
      if (Date.now() - data.timestamp > 600000) {
        return Promise.reject(new Error())
      }
      response.json(data)
    })
    .catch(() => make
      .get({
        'uri': 'https://streamguard.cc/api/videos.json',
        'gzip': true,
        'json': true,
        'timeout': 1000,
        'qs': {
          'kinopoisk_id': request.body.query,
          'api_token': process.env.MOONWALK_CC
        }
      })
      .then(data => {
        if (Array.isArray(data) === false || data.length === 0) { // ERROR
          return response.json({
            'error': {
              'text': 'По данному запросу ничего не нашлось.'
            }
          })
        }
        const temp = {
          'material': {
            'title': data[0].title_ru.split(' (')[0],
            'original_title': data[0].title_en.split(' (')[0],
            'year': data[0].year,
            'tagline': data[0].material_data.tagline,
            'description': data[0].material_data.description.replace(new RegExp('\\n', 'g'), ' ').replace(new RegExp('\\r', 'g'), '').replace(new RegExp('\\s{2,}', 'g'), ' '),
            'age_restriction': data[0].material_data.age,
            'genres': data[0].material_data.genres,
            'actors': data[0].material_data.actors,
            'directors': data[0].material_data.directors,
            'studios': data[0].material_data.studios,
            'kinopoisk': {
              'rating': data[0].material_data.hasOwnProperty('kinopoisk_rating') === true && data[0].material_data.kinopoisk_rating > 0 ? data[0].material_data.kinopoisk_rating : null,
              'votes': data[0].material_data.hasOwnProperty('kinopoisk_votes') === true && data[0].material_data.kinopoisk_votes > 0 ? data[0].material_data.kinopoisk_votes : null
            },
            'imdb': {
              'rating': data[0].material_data.hasOwnProperty('imdb_rating') === true && data[0].material_data.imdb_rating > 0 ? data[0].material_data.imdb_rating : null,
              'votes': data[0].material_data.hasOwnProperty('imdb_votes') === true && data[0].material_data.imdb_votes > 0 ? data[0].material_data.imdb_votes : null
            },
            'duration': data[0].hasOwnProperty('duration') === true ? data[0].duration : -1
          },
          'timestamp': Date.now()
        }
        return fs
          .writeJSON(path.resolve('cache', cache), temp)
          .then(() => response.json(temp))
      })
      .catch(() => response.json({
        'error': {
          'text': 'По данному запросу ничего не нашлось.'
        }
      }))
    )
})

app.post('/api/getMaterialsByName', (request, response) => {
  if (request.body.hasOwnProperty('query') === false) {
    return response.json({
      'error': true
    })
  }
  make
    .get({
      'uri': 'https://streamguard.cc/api/videos.json',
      'gzip': true,
      'json': true,
      'timeout': 1000,
      'qs': {
        'title': request.body.query,
        'api_token': process.env.MOONWALK_CC
      }
    })
    .then(data => {
      if (Array.isArray(data) === false || data.length === 0) { // ERROR
        return response.json({
          'error': {
            'text': 'По данному запросу ничего не нашлось.'
          }
        })
      }
      const kinopoiskIds = new Set()
      const materials = []
      data.forEach(material => {
        if (material.kinopoisk_id !== null) {
          if (kinopoiskIds.has(material.kinopoisk_id) === false) {
            kinopoiskIds.add(material.kinopoisk_id)
            materials.push({
              'id': material.kinopoisk_id,
              'title': material.title_ru.split(' (')[0],
              'original_title': material.title_en.split(' (')[0],
              'year': material.year,
              'kinopoisk': {
                'rating': material.material_data.hasOwnProperty('kinopoisk_rating') === true && material.material_data.kinopoisk_rating > 0 ? material.material_data.kinopoisk_rating : null,
                'votes': material.material_data.hasOwnProperty('kinopoisk_votes') === true && material.material_data.kinopoisk_votes > 0 ? material.material_data.kinopoisk_votes : null
              },
              'imdb': {
                'rating': material.material_data.hasOwnProperty('imdb_rating') === true && material.material_data.imdb_rating > 0 ? material.material_data.imdb_rating : null,
                'votes': material.material_data.hasOwnProperty('imdb_votes') === true && material.material_data.imdb_votes > 0 ? material.material_data.imdb_votes : null
              }
            })
          }
        }
      })
      const temp = {
        'materials': materials
      }
      response.json(temp)
    })
    .catch(() => response.json({
      'error': {
        'text': 'По данному запросу ничего не нашлось.'
      }
    }))
})

app.post('/api/getYouTubeTrailersByName', (request, response) => {
  if (request.body.hasOwnProperty('query') === false) {
    return response.json({
      'error': true
    })
  }
  const cache = `getMaterialById.${request.body.query}.json`
  fs
    .readJSON(path.resolve('cache', cache))
    .then(data => {
      if (Date.now() - data.timestamp > 600000) {
        return Promise.reject(new Error())
      }
      response.json(data)
    })
    .catch(() => make
      .get({
        'uri': 'https://www.googleapis.com/youtube/v3/search',
        'gzip': true,
        'json': true,
        'timeout': 1000,
        'qs': {
          'q': `${request.body.query} трейлер`,
          'key': process.env.YOUTUBE,
          'part': 'snippet',
          'type': 'video',
          'maxResults': 5
        }
      })
      .then(data => {
        if (data.hasOwnProperty('error') === true) {
          return response.json({
            'error': {
              'text': 'По данному запросу ничего не нашлось.'
            }
          })
        }
        const temp = {
          'trailers': data.items.map(item => ({
            'id': item.id.videoId,
            'hostname': 'www.youtube-nocookie.com',
            'pathname': `/embed/${item.id.videoId}/`,
            'query': {
              'modestbranding': 0,
              'rel': 0,
              'showinfo': 0,
              'controls': 2
            },
            'title': item.snippet.title,
            'channel': item.snippet.channelTitle
          })),
          'timestamp': Date.now()
        }
        return fs
          .writeJSON(path.resolve('cache', cache), temp)
          .then(() => response.json(temp))
      })
      .catch(() => response.json({
        'error': {
          'text': 'По данному запросу ничего не нашлось.'
        }
      }))
    )
})

app.listen(process.env.PORT)
