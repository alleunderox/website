const { src, dest, parallel } = require('gulp')
const csso = require('gulp-csso')
const babel = require('gulp-babel')
const concat = require('gulp-concat')

const css = () => src('dev/css/*')
  .pipe(csso())
  .pipe(dest('static/css'))

const js = () => src('dev/js/*')
  .pipe(concat('index.js'))
  .pipe(babel())
  .pipe(dest('static/js'))

exports.js = js
exports.css = css
exports.default = parallel(css, js)
